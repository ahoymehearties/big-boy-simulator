﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigBoySimulator
{
    internal class Room
    {
        public string Name { get; protected set; }
        public string Description { get; protected set; }

        public Scratchpad[] scratchpads = new Scratchpad[] { new Scratchpad(), new Scratchpad()}; 

        public Dictionary<Actions, Room> Exits = new Dictionary<Actions, Room>();
        public static Dictionary<string, Room> ListOfRooms = new Dictionary<string, Room>()
        {
            { "office", new Room("Office", "You find yourself indoors in a large room. There is a bed, a desk, and two large chests that take up a lot of space. In the western side of the room is the litterbox.") },
            { "westernHallway", new Room("Western Hallway", "You are in a long, featureless room. There's nothing but scratchable walls, doors to the west, south, and southeast, and even more hallway to the east.") },
            { "foyer", new Room("Foyer", "You are in a large room full of boxes and scratchable walls. There is a window, a large piece of furniture, and a door.") },
            { "kitchen", new Room("Kitchen", "You are in a room full of dark furniture. There are few walls to scratch here, but it smells delicious. You think this is where they keep the food.") },
            { "dadasOffice", new Room("Dada's Office", "You are in a small room. It smells a bit funky. There's a bunch of garbage around, and some very scratchable looking foam panels. There is also a desk, but it's cluttered with garbage.") },
            { "livingRoom", new Room("Living Room", "You enter a strange room with a deep sense of foreboding. It's interesting and there are lots of places to hide, but it feels very, very wrong to be here.") },
            { "bathroom", new Room("Bathroom", "A small room full of things that are not very scratchable. The ground here is cold. There is a smooth appliance to your right that you innately, and other strange contraptions. You sense the smell of old Poop Poops.") },
            { "bedroom", new Room("Bedroom", "There is a large human bed in this room. Everything looks very dark. Somehow, you are drawn to stay here, though you get the feeling something horrifying goes on here.") },
            { "easternHallway", new Room("Eastern Hallway", "This part of the hallway is strange. The scratachable walls appear to be covered up by some smooth sheets. There are strange drawings on them. You believe they telling the story of you and the cat you sat in the dark expanse with.") },
            { "litterBox", new Room("The Litterbox", "You are in the litterbox.") }
        };

       




        public Room(string name, string description)
        {
            Name = name;
            Description = description;
            
        }
    }
}
