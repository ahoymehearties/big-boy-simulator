﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigBoySimulator
{
    internal class Scratchpad
    {
        private int scratchability = 100;
        public int Scratchability { get { return scratchability; } }

        public Scratchpad()
        {
            scratchability = 100;
        }

        public void Scratch()
        {
            this.scratchability--;
        }
    }
}
