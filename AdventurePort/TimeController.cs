﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigBoySimulator
{
    static class TimeController
    {
        private static DateTime time = new DateTime(2022, 6, 16, 12, 00, 0); 

        public static string Time { get { return time.ToString("HH:mm"); } }

        public static DateTime FullTime { get { return time; } }

        public static void AddMinutes(double minutes)
        {
            time = time.AddMinutes(minutes);
        }

        public static void AddHours(double hours)
        {
            time = time.AddHours(hours);
        }

        public static bool HungerTime(BigBoy bigboy)
        {
            if (time.Hour >= 6 && time.Hour < 8)
            {
                bigboy.TummyFull = false;
                return true;
            }
            else if (time.Hour >= 13 && time.Hour <= 16)
            {
                bigboy.TummyFull = false;
                return true;
            }
            return false;
        }

        public static bool IsBreakfastTime()
        {
            if (time.Hour >= 8 && time.Hour < 9)
            {
                return true;
            }
            return false;
        }

        public static bool IsWunchTime()
        {
            if (time.Hour >= 15 && time.Hour < 16)
            {
                
                return true;
            }
            return false;
        }
        public static bool IsDinnerTime()
        {
            if (time.Hour >= 20 && time.Hour < 21)
            {
                return true;
            }
            return false;
        }

        public static bool IsFoodTime()
        {
            if (IsBreakfastTime() || IsWunchTime() || IsDinnerTime())
            {
                return true;
            }
            return false;
        }
    }
}
