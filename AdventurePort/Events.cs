﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigBoySimulator
{
    static class Events
    {
        static Random random = new Random();
        public static void DadaInvasion(BigBoy bigboy)
        {
            int volatility = random.Next(-10, 10);
            if (volatility == -10)
            {
                Console.WriteLine("Dada enters the room doing a strange interpretive dance while staring directly at you. He is casting a magic spell to kill you!");
            }
            if (volatility >= 1)
            {
                Console.WriteLine("Dada approaches you slowly and doesn't look at you. He speaks to you kindly, and you receive lots of butt scratches from him.");
                if (bigboy.HasCrusties)
                {
                    Console.WriteLine("He also makes fun of your crusties.");
                }
            }
            bigboy.ChangeComfortLevel(volatility);
            Console.WriteLine($"You got {volatility} comfort points!");
            bigboy.ShowComfortandTime();
        }

        public static void MamaInvasion(BigBoy bigboy)
        {
            if (bigboy.GetComfortLevel() > 5)
            {
                bigboy.ChangeComfortLevel(2);
            }
        }
    }
}
