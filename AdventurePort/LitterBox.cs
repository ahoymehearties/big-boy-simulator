﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigBoySimulator
{
    static class LitterBox
    {
        static List<Poop> poops = new List<Poop>();
        static private int coveredPoops = 0;
        static private int uncoveredPoops = 0;

        public static void AddPoops(Poop poop, BigBoy bigboy)
        {
            poops.Add(poop);
            CheckCovered();
            PoopTally();
            if (uncoveredPoops >= 5 || poops.Count >= 10)
            {
                Console.WriteLine("It's starting to stink in here. Maybe you should consider not being so lazy and cover your poop.");
                bigboy.ChangeComfortLevel(-2);
            }
            if (uncoveredPoops >= 8 || poops.Count >= 12)
            {
                Console.WriteLine("Things are getting pretty yucky in here. Someone should cover their poop.");
                bigboy.ChangeComfortLevel(-4);
            }

            if (uncoveredPoops >= 10 || poops.Count >= 14)
            {
                Console.WriteLine("Stop pooping! You don't have to poop so much!");
                bigboy.ChangeComfortLevel(-6);
            }
        }


        public static void CheckCovered()
        {
            coveredPoops = 0;
            uncoveredPoops = 0;
            foreach (Poop p in poops)
            {
                if (p.IsCovered())
                {
                    coveredPoops++;
                }
                else if (!p.IsCovered())
                {
                    uncoveredPoops++;
                }
            }
        }

        public static void CoverAllPoops()
        {
            Poop.CoverPoop();

        }

        public static List<Poop> GetPoopList()
        {
            return poops;
        }

        public static void PoopTally()
        {
            Console.WriteLine($"The litterbox now has {poops.Count} poop poops. {coveredPoops} of them are covered and {uncoveredPoops} are uncovered.");
        }


    }
}
