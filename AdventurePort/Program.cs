﻿using BigBoySimulator;


BigBoy bigboy = new BigBoy();


static void Look()
{
    Console.WriteLine(Controls.currentRoom.Name);
    Console.WriteLine(Controls.currentRoom.Description);
}


Console.WriteLine($"You are a cat. All you knew were the mean streets of a temperate island.\n\nYou were kicked and laughed at by bearded humans with funny hats. They yelled at you in gibberish human language.\n\n You learned to skulk in shadows, hiding in dumpsters and small spaces. You came out for one thing, and one thing alone: food. Food! Glorious food! You learned to be quick, stealing morsels wherever you could with vicious hunger. In fact, you grew to be quite fat, and other cats knew to stay out of your way.\n\nAt least, until the crazy human female arrived.\n\nShe snatched your kind up like candy, and where she sent them, you had no idea, but you never saw them again. She seemed quite friendly, giving you lots and lots of food. You didn't have to hunt. You were happy.\n\nBut it was all a massive deception, because soon, you found yourself shoved into a cage, rattled about for hours, and thrown into a cold, vast expanse where you heard nothing but the wails of your felled damned and a loud din.\n\nYour ears hurt from the pressure, and you were certain that you were being taken to be turned into food for some other creature.\n\nMany humans handled your cage after that, jostling you and shaking you around. You rode on strange moving black carpet, until you were snatched up by the crazy human.\n\nYou were brought into a quiet room, and one of the crazy human from home grabbed you by the scruff of your neck and shoved you into a different cage.\n\nYou hissed at her, but she didn't see things quite the way you did.\n\nAfter that, a strange human female took your cage very gently. You were terrified, and hid in the corner.\n\nThankfully, she covered the cage with a blanket so you wouldn't have to see the confusing sights around you.\n\nYou prayed for death.\n\nThen, finally, quiet. Your cage was opened, and you witnessed a strange new world.");

/* This stuff below sucks
 * I can do way better than this trash
 * Also, we don't need "Exits" -- change to DOORS that lead to ROOMS. Linking lists! you're doing it already!
 */
var office = Room.ListOfRooms["office"];
var westernHallway = Room.ListOfRooms["westernHallway"];
var foyer = Room.ListOfRooms["foyer"];
var kitchen = Room.ListOfRooms["kitchen"];
var dadasOffice = Room.ListOfRooms["dadasOffice"];
var livingroom = Room.ListOfRooms["livingRoom"];
var bathroom = Room.ListOfRooms["bathroom"];
var bedroom = Room.ListOfRooms["bedroom"];
var easternHallway = Room.ListOfRooms["easternHallway"];
var litterBox = Room.ListOfRooms["litterBox"];

office.Exits.Add(Actions.South, westernHallway);
office.Exits.Add(Actions.West, litterBox);
litterBox.Exits.Add(Actions.North, office);
westernHallway.Exits.Add(Actions.North, office);
westernHallway.Exits.Add(Actions.West, foyer);
westernHallway.Exits.Add(Actions.South, dadasOffice);
westernHallway.Exits.Add(Actions.SouthEast, kitchen);
westernHallway.Exits.Add(Actions.East, easternHallway);
foyer.Exits.Add(Actions.East, easternHallway);
foyer.Exits.Add(Actions.North, livingroom);
livingroom.Exits.Add(Actions.South, foyer);
dadasOffice.Exits.Add(Actions.North, westernHallway);
kitchen.Exits.Add(Actions.North, westernHallway);
easternHallway.Exits.Add(Actions.West, westernHallway);
easternHallway.Exits.Add(Actions.North, bedroom);
easternHallway.Exits.Add(Actions.South, bathroom);
bathroom.Exits.Add(Actions.North, westernHallway);
bedroom.Exits.Add(Actions.South, westernHallway);


// The next two lines are also trash and shouldn't be in main. 
Controls.currentRoom = Room.ListOfRooms["office"];
Look();
while (Controls.playing)
{
    Console.Write("What would you like to do: ");
    var decision = Console.ReadLine();

    var action = Controls.ConvertStringToDirection(decision);

    if (decision == "q" || decision == "quit" || decision == "exit")
    {
        Controls.playing = false;
    }
    else
    {
        Controls.Go(action, bigboy);
    }
}