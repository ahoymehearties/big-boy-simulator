﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigBoySimulator
{
    enum FoodTypes
    {
        Pute,
        Kangaru,
        Gans,
        Ente,
        Kalb,
        HairAndSkin,
        Huhn,
        Rind
    }

    enum SnackTypes
    {
        Dreamies,
        MeatStick,
        GrassTreat,
        CatnipTreat,
        RelaxTreat
    }
    internal class Food
    {
        Random random = new Random();
        
        private string _foodName;
        private string _snackName;

        Array foodvalues = Enum.GetValues(typeof(FoodTypes));
        public string FoodName
        {
            get
            {
                return _foodName;
            }
            set
            {
                _foodName = typeof(FoodTypes).Name;
            }
        }

        public string SnackName
        {
            get
            {
                return _snackName;
            }
            set
            {
                _snackName = typeof(SnackTypes).Name;
            }
        }


    }
}
